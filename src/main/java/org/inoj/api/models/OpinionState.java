package org.inoj.api.models;

public enum OpinionState {
    AGAINST,
    SUPPORTIVE,
    NEUTRAL,
    NOT_SET
}
