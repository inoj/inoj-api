package org.inoj.api.controllers;

import org.inoj.api.exceptions.ItemNotFoundException;
import org.inoj.api.models.Politic;
import org.inoj.api.repositories.PoliticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class PoliticController {

    @Autowired
    public PoliticRepository politicRepository;

    @GetMapping("/politics")
    public List<Politic> getAll() {
        return politicRepository.findAll();
    }

    @PostMapping("/politics")
    public Politic add(@RequestBody Politic politic) {
        return politicRepository.save(politic);
    }


    @GetMapping("/politics/{id}")
    Politic getById(@PathVariable int id) {

        return politicRepository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException(id));
    }

    @PutMapping("/politics/{id}")
    Politic updatePolitic(@RequestBody Politic newPolitic, @PathVariable Integer id) {

        return politicRepository.findById(id)
                .map(employee -> {
                    employee.setFirstname(newPolitic.getFirstname());
                    employee.setLastname(newPolitic.getLastname());
                    employee.setFaction(newPolitic.getFaction());
                    return politicRepository.save(employee);
                })
                .orElseGet(() -> {
                    newPolitic.setId(id);
                    return politicRepository.save(newPolitic);
                });
    }

    @DeleteMapping("/politics/{id}")
    void deletePolitic(@PathVariable Integer id) {
        politicRepository.deleteById(id);
    }
}
