package org.inoj.api.repositories;

import org.inoj.api.models.Politic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PoliticRepository extends JpaRepository<Politic, Integer> {
}
