package org.inoj.api.exceptions;

public class ItemNotFoundException extends RuntimeException {
    public ItemNotFoundException(Integer id) {
        super("Could not find employee " + id);
    }
}
