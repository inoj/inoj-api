package org.inoj.api.models;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PoliticOpinion implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn
    private Politic politic;

    @Id
    @ManyToOne
    @JoinColumn
    private Opinion opinion;

    private OpinionState opinionState;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PoliticOpinion)) return false;
        PoliticOpinion that = (PoliticOpinion) o;
        return Objects.equals(opinion.getId(), that.opinion.getId()) &&
                Objects.equals(politic.getId(), that.politic.getId()) &&
                Objects.equals(opinionState, that.opinionState);
    }

    @Override
    public int hashCode() {
        return Objects.hash(opinion.getId(), politic.getId(), opinionState);
    }
}
