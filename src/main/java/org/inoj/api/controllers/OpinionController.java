package org.inoj.api.controllers;

import org.inoj.api.exceptions.ItemNotFoundException;
import org.inoj.api.models.Opinion;
import org.inoj.api.repositories.OpinionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class OpinionController {

    @Autowired
    public OpinionRepository opinionRepository;

    @GetMapping("/opinions")
    public List<Opinion> getAll() {
        return opinionRepository.findAll();
    }

    @PostMapping("/opinions")
    public Opinion add(@RequestBody Opinion opinion) {
        return opinionRepository.save(opinion);
    }


    @GetMapping("/opinions/{id}")
    Opinion getById(@PathVariable int id) {

        return opinionRepository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException(id));
    }

    @PutMapping("/opinions/{id}")
    Opinion updateOpinion(@RequestBody Opinion newOpinion, @PathVariable Integer id) {

        return opinionRepository.findById(id)
                .map(employee -> {
                    employee.setCode(newOpinion.getCode());
                    employee.setDescription(newOpinion.getDescription());
                    return opinionRepository.save(employee);
                })
                .orElseGet(() -> {
                    newOpinion.setId(id);
                    return opinionRepository.save(newOpinion);
                });
    }

    @DeleteMapping("/opinions/{id}")
    void deleteOpinion(@PathVariable Integer id) {
        opinionRepository.deleteById(id);
    }
}
