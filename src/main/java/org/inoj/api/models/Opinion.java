package org.inoj.api.models;

import java.io.Serializable;
import java.util.Set;

import lombok.*;
import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Opinion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "code")
    private String code;
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "opinion", cascade = CascadeType.DETACH)
    private Set<PoliticOpinion> politicOpinion;
}
